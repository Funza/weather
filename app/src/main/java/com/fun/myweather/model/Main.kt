package com.`fun`.myweather.model

import com.google.gson.annotations.SerializedName
import lombok.Data
import lombok.Value

@Data
@Value
class Main {
    @SerializedName("temp")
    var temp: Double? = null

    @SerializedName("feelsLike")
    var feelsLike: Double? = null

    @SerializedName("tempMin")
    var tempMin: Double? = null

    @SerializedName("tempMax")
    var tempMax: Double? = null

    @SerializedName("pressure")
    var pressure: Int? = null

    @SerializedName("humidity")
    var humidity: Int? = null
}