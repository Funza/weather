package com.`fun`.myweather.injection

import android.app.Application
import com.`fun`.myweather.AppApplication
import com.`fun`.myweather.webservice.ServiceModule


import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, ActivityBuilder::class,
        AppModule::class, ViewModelBuilderModule::class, ServiceModule::class]
)
interface AppComponent : AndroidInjector<AppApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    override fun inject(application: AppApplication)
}