package com.`fun`.myweather.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.`fun`.myweather.R
import com.`fun`.myweather.helpers.NetworkHelper
import com.`fun`.myweather.helpers.utils.DataState
import com.`fun`.myweather.helpers.utils.DescriptionType
import com.`fun`.myweather.helpers.utils.ResourceDataStatus
import com.`fun`.myweather.injection.ViewModelFactory
import com.`fun`.myweather.model.WeatherDt
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class MainActivity : BaseActivity() {

    private val URL: String = "https://openweathermap.org/img/wn/"

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var mainViewModel: MainViewModel

    override val contentResourceId: Int
        get() = R.layout.activity_main

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainViewModel = this.viewModelFactory.let {
            ViewModelProvider(this, it).get(MainViewModel::class.java)
        }

        initCheck()

        val weatherObserver = Observer<ResourceDataStatus<WeatherDt?>> { weatherResource ->
            when (weatherResource.state) {
                DataState.LOADING -> {
                    progress_bar.show()
                }
                DataState.SUCCESS -> {
                    if (weatherResource != null) {
                        val description: String =
                            weatherResource.data?.weather?.get(0)?.description.toString()
                        val df: DateFormat = SimpleDateFormat("EEEE, dd MMMM yy")
                        val date: String = df.format(Calendar.getInstance().time)
                        val icon: String =
                            weatherResource.data?.weather?.get(0)?.icon.toString() + ".png"
                        time.text = date
                        temp.text = DecimalFormat("#").format(weatherResource.data?.main?.temp)
                        conditions_title.text = description
                        current_location.text = weatherResource.data?.name

                        //Download weather icons for each description
                        Glide.with(this)
                            .load("$URL$icon")
                            .into(weather_icon)

                        view_group.visibility = View.VISIBLE

                        when (description) {
                            DescriptionType.CLEAR_SKIES -> {
                                main_view.setBackgroundColor(R.color.clear_sunny)
                            }
                            DescriptionType.RAINY -> {
                                main_view.setBackgroundColor(R.color.cloudy)
                            }
                            DescriptionType.WINDY -> {
                                main_view.setBackgroundColor(R.color.clear_skies)
                            }
                            DescriptionType.OVERCAST_CLOUDS -> {
                                main_view.setBackgroundColor(R.color.cloudy)
                            }
                            DescriptionType.BROKEN_CLOUDS -> {
                                main_view.setBackgroundColor(R.color.clear_gray)
                            }
                            DescriptionType.FEW_CLOUDS -> {
                                main_view.setBackgroundColor(R.color.few_clouds)
                            }
                            DescriptionType.SNOW -> {
                                main_view.setBackgroundColor(R.color.snow)
                            }
                            DescriptionType.SCATTERED_CLOUDS -> {
                                main_view.setBackgroundColor(R.color.scattered_clouds)
                            }
                            DescriptionType.THUNDER -> {
                                main_view.setBackgroundColor(R.color.clear_gray)
                            }
                            DescriptionType.MIST -> {
                                main_view.setBackgroundColor(R.color.mist)
                            }
                        }
                        progress_bar.hide()
                    }
                }
                DataState.ERROR -> {
                    progress_bar.hide()
                    Toast.makeText(this, weatherResource.error, Toast.LENGTH_LONG).show()
                }
            }
        }
        mainViewModel.getWeatherLiveData().observe(this, weatherObserver)

        refresh.setOnClickListener {
            view_group.visibility = View.GONE
            progress_bar.show()
            initCheck()
        }
    }

    private fun initCheck() {
        if (NetworkHelper.isNetworkAvailable(this)) {
            setupPermissions()
        } else {
            showErrorDialog(getString(R.string.error_connectivity))
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if ((checkSelfPermission(
                            this@MainActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED)
                    ) {

                        mainViewModel.subscribeToCurrentWeather()
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

    private fun setupPermissions() {
        if (checkSelfPermission(
                this@MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this@MainActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                ActivityCompat.requestPermissions(
                    this@MainActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            } else {
                ActivityCompat.requestPermissions(
                    this@MainActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        } else {
            mainViewModel.subscribeToCurrentWeather()
        }
    }
}
