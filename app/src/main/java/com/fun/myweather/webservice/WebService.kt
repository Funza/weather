package com.`fun`.myweather.webservice

import com.`fun`.myweather.model.WeatherDt
import io.reactivex.Observable
import retrofit2.Response

interface WebService {

    fun getUserCurrentWeather(
        latitude: String,
        longitude: String
    ): Observable<Response<WeatherDt>>

}