package com.`fun`.myweather.ui

import android.R
import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.`fun`.myweather.injection.ActivityBuilder
import com.`fun`.myweather.injection.ActivityBuilder_BindMainActivity
import dagger.android.AndroidInjection

abstract class BaseActivity : AppCompatActivity() {
    abstract val contentResourceId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentResourceId)
        AndroidInjection.inject(this)
    }

    fun setUpActionBar(title: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = title
    }

    fun showErrorDialog(message: String) {
        val dlg: AlertDialog.Builder = AlertDialog.Builder(this)
        dlg.setTitle("Note")
        dlg.setMessage(message)
        dlg.setPositiveButton(
            getString(R.string.ok)
        ) { _, _ ->
            finish()
        }.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}