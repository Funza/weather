package com.`fun`.myweather.helpers.utils

enum class DataState {
    SUCCESS, ERROR, LOADING
}