package com.`fun`.myweather.model

import com.google.gson.annotations.SerializedName
import lombok.Data
import lombok.Value

@Data
@Value
class Coord {
    @SerializedName("lon")
    var lon: Double? = null

    @SerializedName("lat")
    var lat: Double? = null
}