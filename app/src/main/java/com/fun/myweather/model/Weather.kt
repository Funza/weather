package com.`fun`.myweather.model

import com.google.gson.annotations.SerializedName
import lombok.Data
import lombok.Value

@Data
@Value
class Weather {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("main")
    var main: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("icon")
    var icon: String? = null
}