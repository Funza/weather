package com.`fun`.myweather.injection

import androidx.lifecycle.ViewModel
import com.`fun`.myweather.ui.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@SuppressWarnings("ALL")
@Module
abstract class ViewModelBuilderModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel?): ViewModel?

}