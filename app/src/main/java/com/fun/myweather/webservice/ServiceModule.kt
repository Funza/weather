package com.`fun`.myweather.webservice

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ServiceModule {
    @Provides
    fun providesUserService(context: Context?): WebService {
        return WebServiceImpl(context)
    }
}