package com.`fun`.myweather.webservice

import com.`fun`.myweather.model.WeatherDt
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("weather")
    fun getUserCurrentWeather(
        @Query("lat") lat: String?,
        @Query("lon") lon: String,
        @Query("appid") appid: String,
        @Query("units") units: String
    ): Observable<Response<WeatherDt>>
}