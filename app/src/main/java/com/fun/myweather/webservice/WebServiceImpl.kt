package com.`fun`.myweather.webservice

import android.content.Context
import com.`fun`.myweather.model.WeatherDt

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class WebServiceImpl @Inject constructor(private val context: Context?) : WebService {

    companion object {
        private const val UNITS =
            "metric"
        private const val KEY =
            "f078ee1dc69f4030000271c30ed8f594"
        private const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
        private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        private val api = retrofit.create(
            Api::class.java
        )
    }

    override fun getUserCurrentWeather(
        latitude: String,
        longitude: String
    ): Observable<Response<WeatherDt>> {
        return api.getUserCurrentWeather(latitude, longitude, KEY, UNITS)
    }
}