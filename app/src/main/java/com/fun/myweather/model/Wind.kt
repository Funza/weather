package com.`fun`.myweather.model

import com.google.gson.annotations.SerializedName
import lombok.Data
import lombok.Value

@Data
@Value
class Wind {
    @SerializedName("speed")
    var speed: Double? = null

    @SerializedName("deg")
    var deg: Int? = null
}