package com.`fun`.myweather.ui

import android.app.Application
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.`fun`.myweather.helpers.LocationTracker
import com.`fun`.myweather.helpers.utils.ResourceDataStatus
import com.`fun`.myweather.model.WeatherDt
import com.`fun`.myweather.webservice.WebService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject


class MainViewModel @Inject constructor(application: Application, private val service: WebService) :
    AndroidViewModel(application) {

    private val weatherLiveData: MutableLiveData<ResourceDataStatus<WeatherDt?>> =
        MutableLiveData<ResourceDataStatus<WeatherDt?>>()

    private val compositeDisposable = CompositeDisposable()

    private val locationTracker: LocationTracker =
        LocationTracker(application.applicationContext)

    fun getWeatherLiveData(): LiveData<ResourceDataStatus<WeatherDt?>> {
        return weatherLiveData
    }

    fun subscribeToCurrentWeather() {
        Log.d("Location track..........", locationTracker.getLatitude().toString())
        val disposable: Disposable = service.getUserCurrentWeather(locationTracker.getLatitude().toString(), locationTracker.getLongitude().toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { weatherLiveData.value = ResourceDataStatus.postLoading() }
            .subscribe(
                { response ->
                    weatherLiveData.value = ResourceDataStatus.postSuccess(response.body())
                    Log.d("Success..........", response.body()?.main?.temp.toString())
                }, { throwable ->
                    weatherLiveData.value =
                        throwable.message?.let { ResourceDataStatus.postError(it) }
                    Log.d("Error..........", throwable.message.toString())
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}